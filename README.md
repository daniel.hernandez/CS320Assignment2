CS320 Assignment #3
===================

Programs
--------
**_Names_**

prog2_1.sh

prog2_2.sh

prog2_3.sh

**_Description_**

prog2_1.sh:

This program searches through two files to find a specific item (name of a student that has
scored 100 on all his exams) and outputs that user's information based on those files.

prog2_2.sh:

This program searches through the git repository of said student and copies, and renames, 4 
different C programs with the correct file extension onto the user's current directory.

prog2_3.sh:

This program determines which assisngments corresponds with the correct descripition of that
assignment. Outputs which file goes to which assisngment.


**_Compiling_**

Program "prog2_1.sh": 

./prog2_1.sh

Program "prog2_2.sh": 

./prog2_2.sh

Program "prog2_3.sh": 

./prog2_3.sh


Ojective
--------
Be able to learn and adapt in an unfamiliar environment. In this case, we are learning and
searching to find solutions for programs related to BASH.
