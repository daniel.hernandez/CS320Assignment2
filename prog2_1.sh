#!/bin/bash

echo -e "Assignment #2-1, Daniel Hernandez, dhernandez92105@yahoo.com\n"

grades=`grep 100,100,100 Grades`

firstname=`echo $grades | cut -d "," -f1`
lastname=`echo $grades | cut -d "," -f2`

echo -e $firstname  $lastname "\n"

loginInfo=`grep $lastname Logins`

username=`echo $loginInfo | cut -d "," -f2`
password=`echo $loginInfo | cut -d "," -f3`

echo -e  $username"\n\n"$password
