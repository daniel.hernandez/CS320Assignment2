#!/bin/bash

echo -e "Assignment #2-3, Daniel Hernandez, dhernandez92105@yahoo.com\n"

file1=`grep -ir "%.3f" cs320assignment3`
name1=`basename ${file1} | sed 's/://'`
echo -e $name1 "Assignment #1\n"

file2=`grep -ir "%.4f" cs320assignment3`
name2=`basename ${file2} | sed 's/://'`
echo -e $name2 "Assignment #2\n"

file3=`grep -ir "%.5f" cs320assignment3`
name3=`basename ${file3} | sed 's/://'`
echo -e $name3 "Assignment #3\n"

file4=`grep -ir "%2.f" cs320assignment3`
name4=`basename ${file4} | sed 's/://'`
echo -e $name4 "Assignment #4\n"
