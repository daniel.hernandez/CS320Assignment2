#!/bin/bash

echo -e "Assignment #2-2, Daniel Hernandez, dhernandez92105@yahoo.com\n"

for files in `grep -irl "stdio" cs320assignment3`
do
    mv $files $files".c"
    cp $files*.c .
    names=`basename $files*.c`
    echo ${names} | sed G
done
